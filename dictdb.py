# dictdb.py by Lessandro Mariano
# https://github.com/lessandro/dictdb

dict_type = dict

try:
    import avltree
    dict_type = avltree.AVLDict
except ImportError:
    pass


class CDict(dict_type):
    """
    Counting dict - a dictionary that keeps track of the number of
    occurrences of its element's values.
    """
    def __init__(self):
        super(CDict, self).__init__()

        # create the reverse dictionary
        self._reverse_dict = CDict.__base__()

    def _inc(self, value):
        if value not in self._reverse_dict:
            self._reverse_dict[value] = 1
        else:
            self._reverse_dict[value] += 1

    def _dec(self, value):
        if self._reverse_dict[value] == 1:
            del self._reverse_dict[value]
        else:
            self._reverse_dict[value] -= 1

    def __setitem__(self, key, value):
        if key in self:
            self._dec(self[key])

        super(CDict, self).__setitem__(key, value)

        self._inc(value)

    def __delitem__(self, key):
        if key in self:
            self._dec(self[key])

        super(CDict, self).__delitem__(key)

    def count(self, value):
        """
        Return the number of occurrences of value in the dictionary.
        """
        return self._reverse_dict.get(value, 0)


class Dictdb(CDict):
    """
    Dictdb - a simple key-value store with transactions

    Transactions are implemented with a list of dictionaries, each dict
    represents the necessary operations to change the database from its current
    state to the state it was at the beginning of the transaction (reverse
    diffs).
    """
    def __init__(self):
        super(Dictdb, self).__init__()

        # transactions as list of reverse diffs
        self._transactions = []

    def _record(self, key):
        """
        Record the operation necessary to return key to its old value.
        """
        if not self._transactions:
            return

        # get the last transaction
        transaction = self._transactions[-1]
        if key in transaction:
            return

        # wrap the value in a tuple so that we can differentiate between
        # deleted keys and None values
        if key in self:
            transaction[key] = (self[key],)
        else:
            transaction[key] = None

    def __setitem__(self, key, value):
        self._record(key)
        super(Dictdb, self).__setitem__(key, value)

    def __delitem__(self, key):
        self._record(key)
        super(Dictdb, self).__delitem__(key)

    def begin(self):
        """
        Begin a transaction.
        """
        self._transactions.append({})

    def rollback(self):
        """
        Rollback the most recent transaction.

        This method simply iterates through the last transaction dict
        modifying the current database to the state it was when the
        transaction started.
        """
        if not self._transactions:
            return False

        # replay the reverse diff log
        transaction = self._transactions.pop()
        for key, value in transaction.iteritems():
            if value:
                super(Dictdb, self).__setitem__(key, value[0])
            else:
                super(Dictdb, self).__delitem__(key)

        return True

    def commit(self):
        """
        Commit all existing transactions.

        In practice this only clears the reverse diff list.
        """
        if not self._transactions:
            return False

        self._transactions = []

        return True
